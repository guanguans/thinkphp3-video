<?php
namespace Home\Controller;
use Think\Controller;
class SearchController extends Controller {
    /**
	*	网站搜索
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function search(){
    	$class = I('post.class_name', 0, 'int');
    	if ($class) {
    		$map['post_title'] = array('LIKE', '%'.I('name').'%');
	    	$map['post_status'] = 1;
	    	$posts_news = M('posts_news'); // 实例化User对象
	        $count = $posts_news->where($map)->count();// 查询满足要求的总记录数
	        $Page = new \Think\Page($count,2);// 实例化分页类 传入总记录数和每页显示的记录数(25)
	        $show = $Page->show();// 分页显示输出// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
	        $data = $posts_news->where($map)->order('istop desc, post_hits desc, post_like desc, post_collect desc, post_date desc')->limit($Page->firstRow.','.$Page->listRows)->field('id, post_title, post_date, post_hits')->select();
	        $this->assign('data',$data);// 赋值数据集
	        $this->assign('page',$show);// 赋值分页输出$this->display(); // 输出模板
	        $this->assign($_POST);
	        $this->display(':news_list');

    	} else {
	        $map['post_title'] = array('LIKE', '%'.I('name').'%');
	        $map['post_status'] = 1;
	        $posts_video = M('posts_video'); // 实例化User对象
	        $count = $posts_video
	    		->join('guanguan_term_relationships_video ON id = object_id')
	    		->where($map)
	    		->count();
	        $Page = new \Think\Page($count,1);// 实例化分页类 传入总记录数和每页显示的记录数(25)
	        $show = $Page->show();// 分页显示输出
	        $data = $posts_video
	        	->join('guanguan_term_relationships_video ON id = object_id')
	        	->where($map)
	        	->limit($Page->firstRow.','.$Page->listRows)
	        	->select();
	        $this->assign('term_data',$term_data);// 赋值数据集
	        $this->assign('data',$data);// 赋值数据集
	        $this->assign('page',$show);// 赋值分页输出
	        $this->assign($_POST);
	        $this->display(':video_list'); // 输出模板
    	}
    }

    /**
	*	网站搜索清空
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function clear(){
    	$class = I('class_name', 0, 'int');
    	if ($class) {
	    	$map['post_status'] = 1;
	    	$posts_news = M('posts_news'); // 实例化User对象
	        $count = $posts_news->where($map)->count();// 查询满足要求的总记录数
	        $Page = new \Think\Page($count,2);// 实例化分页类 传入总记录数和每页显示的记录数(25)
	        $show = $Page->show();// 分页显示输出// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
	        $data = $posts_news->where($map)->order('istop desc, post_hits desc, post_like desc, post_collect desc, post_date desc')->limit($Page->firstRow.','.$Page->listRows)->field('id, post_title, post_date, post_hits')->select();
	        $this->assign('data',$data);// 赋值数据集
	        $this->assign('page',$show);// 赋值分页输出$this->display(); // 输出模板
	        $list['class_name'] = $class;
	        $this->assign($list);
	        $this->display(':news_list');
    	} else {
	        $map['post_status'] = 1;
	        $posts_video = M('posts_video'); // 实例化User对象
	        $count = $posts_video
	    		->join('guanguan_term_relationships_video ON id = object_id')
	    		->where($map)
	    		->count();
	        $Page = new \Think\Page($count,1);// 实例化分页类 传入总记录数和每页显示的记录数(25)
	        $show = $Page->show();// 分页显示输出
	        $data = $posts_video
	        	->join('guanguan_term_relationships_video ON id = object_id')
	        	->where($map)
	        	->limit($Page->firstRow.','.$Page->listRows)
	        	->select();
	        $this->assign('term_data',$term_data);// 赋值数据集
	        $this->assign('data',$data);// 赋值数据集
	        $this->assign('page',$show);// 赋值分页输出
	        $list['class_name'] = $class;
	        $this->assign($list);
	        $this->display(':video_list'); // 输出模板
    	}
    }

}