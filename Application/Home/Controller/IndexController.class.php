<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {

	/**
	*	网站首页
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function index(){
        $id = I('id', 0, 'int');
    	$slide_cid = M('slide_cat')->where("cat_name='首页轮播图'")->field('cid')->find();
    	$data = M('slide')->where("slide_cid={$slide_cid['cid']} and slide_status=1")->select();
        $list = M('users')->where("id=13")->field('id,user_nicename,mobile,avatar')->find();
        $this->assign('slides', $data);
    	$this->assign($list);
        $this->display(':index');
    }

}