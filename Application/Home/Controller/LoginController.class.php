<?php
namespace Home\Controller;
use Think\Controller;
class LoginController extends Controller {

    /**
    *   请求短息验证码
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function send_code(){
        header('Content-Type: text/html; charset=utf-8');
        $mobile = I('mobile');
        $code = mt_rand(1000,9999); 
        $Alidayu = new \Org\Alidayu\Alidayu;
        $Alidayu = $Alidayu->send_code($mobile, "$code");
        // if ($Alidayu['result']['success']) {
        //     $data = array('now'=>time()+60, 'info'=>111 ,'code'=>$code);
        //     $json = json_encode($data);
        //     echo $json;
        // } else {
        //     echo $Alidayu['sub_msg'];
        // }
        $data = array('now'=>time()+60, 'info'=>'success', 'code'=>1234, 'mobile'=>$mobile);
        $json = json_encode($data);
        echo $json;
    }

    /**
    *   用户注册
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function reg(){
        $data['mobile'] = I('post.mobile');
        $data['user_pass'] = I('post.user_pass1');
        $user_pass2 = I('post.user_pass2');
        if ($data['user_pass'] == $user_pass2) {
            $data['user_pass'] = md5( $data['user_pass']);
            $res = M('users')->add($data);
            if ($res) {
                $this->success('注册成功，请登录');
            } else {
                $this->error('注册失败！');
            }
        } else {
            $this->error('两次密码不一样');
        }
    }


    /** 
     * 执行登录
     * author 琯琯 
     * date 2017.4.30
     */
    public function dologin(){
        $name = I("post.mobile");
        if(empty($name)){
            $this->error('账号不能为空');
        }
        $pass = I("post.user_pass");
        if(empty($pass)){
            $this->error('密码不能为空');
        }
        // $verrify = I("post.code");
        // if(empty($verrify)){
        //  $this->error('验证码不能为空');
        // }
        // 验证码
        // if (!$this->check_verify($verrify) ) {
        //  $this->error('验证码不对');
        // }
        $user_m = M('users');
        $data = $user_m->where("mobile='$name'")->find();
        // 用户
        if (!$data) {
            $this->error('账号或密码错误');
        }
        // 状态
        if (!$data['user_status']) {
            $this->error('账号被禁用');
        }
        
        // 加密
        $pass = md5($pass);
        $result = $user_m->where("mobile='$name' and user_pass='$pass'")->find();
        if ($result) {
            //登入成功页面跳转
            session('user_id',$result["id"]);
            session('user_nicename',$result["user_nicename"]);
            session('user_mobile',$result["mobile"]);
            session('user_avatar',$result["avatar"]);
            $new_data['last_login_ip']=get_client_ip(0,true);
            $new_data['last_login_time']=date("Y-m-d H:i:s");
            $user_m->where("mobile='$name' and user_pass='$pass'")->save($new_data);
            cookie("user_nicename",$name,3600*24*30);
            $this->assign($result);
            $this->redirect('index/index', array('id'=>$result['id']));
        } else {
            $this->error('密码错误');
        }
    }

    /** 
     * 退出 
     * author 琯琯 
     * date 2017.4.30
     */ 
    public function logout(){
        session(null); 
        $this->redirect('index/index');
    }

}