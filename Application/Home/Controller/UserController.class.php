<?php
namespace Home\Controller;
use Think\Controller;
class UserController extends Controller {

    /**
    *   基本资料
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function jbzl_index(){
        $id = I('get.id', 0, 'int');
        $data = M('users')->where("id = $id")->find();
        $address = json_decode($data['address'], true);
        $this->assign($data);
        $this->assign('address', $address);
        $this->display(':jbzl');
    }

    /**
    *   基本资料执行修改
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function jbzl_doedit(){
        header("content-type:text/html;charset=utf-8");
        if (IS_POST) {
            $id = I('get.id', 0, 'int');
            $data = I('post.');
            $address['province'] = $data['province'];
            $address['city'] = $data['city'];
            $address['county'] = $data['county'];
            unset($data['province']);
            unset($data['city']);
            unset($data['county']);
            $data['address'] = json_encode($address);
            $res = M('users')->where("id = $id")->save($data);
            if ($res) {
                $this->redirect('jbzl_index', array('id'=>$id));
            } else {
                $this->error('修改失败');
            }
        } else {
            $this->error('非法操作');
        }
       
    }

    /**
    *   头像修改
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function txxg_index(){
        $id = I('get.id', 0, 'int');
        $data = M('users')->where("id = $id")->field('id,user_nicename,mobile,avatar')->find();
        $this->assign($data);
        $this->display(':txxg');
    }

     /**
    *   头像执行修改
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function txxg_doedit(){
        if (IS_POST) {
            $avatar = upload_one($_FILES['avatar']);
            if (!$avatar) {
                $this->error('修改失败');
            } 
            $id = I('get.id');
            $data = I('post.');
            $data['avatar'] = $avatar['savepath'].$avatar['savename'];
            $res = M('users')->where("id=$id")->save($data);
            if ($res) {
                $this->redirect('txxg_index', array('id'=>$id));
            } else {
                $this->error('修改失败');
            }
        }
    }

    /**
    *   密码修改
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function mmxg_index(){
        $id = I('get.id', 0, 'int');
         $data = M('users')->where("id = $id")->field('id,user_nicename,mobile,avatar')->find();
        $this->assign($data);
        $this->display(':mmxg');
    }

    /**
    *   密码修改
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function mmxg_doedit(){
        $mobile['mobile'] = I('get.user_mobile', 0, 'int');
        $mobile['user_pass'] = md5(I('post.pass_old'));
        $user_pass  = md5(I('post.pass_new1'));
        $data['user_pass']  = md5(I('post.pass_new2'));
        if ($data['user_pass'] == $user_pass) {
            $res = M('users')->where($mobile)->field('id')->find();
            if ($res) {
                $result = M('users')->where($mobile)->save($data);
                if ($result) {
                    $this->success('修改成功');        
                } else {
                    $this->error('修改失败');
                }
            } else {
                $this->error('旧密码错误');
            }
        } else {
            $this->error('两次密码不一样');
        }
    }

    /**
    *   我的收藏
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function wdsc_index(){
        $id = I('get.id', 0, 'int');
        $data = M('users')->where("id = $id")->field('id,user_nicename,mobile,avatar')->find();
        $count = M('comments_collect')->where("uid = $id")->count();
        $Page = new \Think\Page($count,1);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        $collect_data = M('comments_collect')->where("uid = $id")->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign($data);
        $this->assign('collect_data', $collect_data);
        $this->assign('page', $show);
        $this->display(':wdsc');
    }

    /**
    *   我的收藏取消
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function collect_delete(){
        $id = I('get.id', 0, 'int');
        $res = M('comments_collect')->where("id = $id")->delete();
        if ($res) {
            $this->redirect('wdsc_index', array('id'=>$_SESSION['user_id']));
        } else {
            $this->error("取消失败");
        }
    }


    
    /**
    *   我的评论
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function wdpl_index(){
        $id = I('get.id', 0, 'int');
        $data = M('users')->where("id = $id")->field('id,user_nicename,mobile,avatar')->find();
        $count = M('comments_comments')->where("uid = $id")->count();
        $Page = new \Think\Page($count,5);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        $collect_data = M('comments_comments')->where("uid = $id")->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign($data);
        $this->assign('collect_data', $collect_data);
        $this->assign('page', $show);
        $this->display(':wdpl');
    }

    // /**
    // *   我的评论删除
    // *   author by 琯琯
    // *   date 2017.4.28
    // */
    // public function comment_delete(){
    //     $id = I('get.id', 0, 'int');
    //     $res = M('comments_comments')->where("id = $id")->delete();
    //     if ($res) {
    //         $this->redirect('wdpl_index', array('id'=>$_SESSION['user_id']));
    //     } else {
    //         $this->error("删除失败");
    //     }
    // }
}