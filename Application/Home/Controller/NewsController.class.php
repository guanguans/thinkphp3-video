<?php
namespace Home\Controller;
use Think\Controller;
class NewsController extends Controller {

	/**
	*	新闻列表
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function index(){
        $posts_news = M('posts_news'); // 实例化User对象
        $count = $posts_news->where('post_status=1')->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,2);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $data = $posts_news->where('post_status=1')->order('istop desc, post_hits desc, post_like desc, post_collect desc, post_date desc')->limit($Page->firstRow.','.$Page->listRows)->field('id, post_title, post_date, post_hits')->select();
        $this->assign('data',$data);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出$this->display(); // 输出模板
        $this->display(':news_list');
    }

    /**
	*	新闻详情
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function info(){
    	$id = I('get.post_id', 0, 'int');
    	$data = M('posts_news')->where("id=$id")->find();
    	$post_hits['post_hits'] = $data['post_hits'] + 1;
    	$res = M('posts_news')->where("id=$id")->save($post_hits);
    	$hot = M('posts_news')->where('post_status=1')->order('istop desc, post_hits desc, post_like desc, post_collect desc, post_date desc')->limit(5)->select();
    	$this->assign($data);
    	$this->assign('hot', $hot);
        $this->display(':news_info');
    }
}