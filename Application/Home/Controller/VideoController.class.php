<?php
namespace Home\Controller;
use Think\Controller;
class VideoController extends Controller {

	/**
	*	视频列表
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function index(){
    	$term_id = I('get.term_id', 0, 'int');
        $posts_video = M('posts_video'); // 实例化User对象
        $map['term_id'] = $term_id;
        $map['post_status'] = 1;
        $count = $posts_video
    		->join('guanguan_term_relationships_video ON id = object_id')
    		->where($map)
    		->count();
        $Page = new \Think\Page($count,1);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        $data = $posts_video
        	->join('guanguan_term_relationships_video ON id = object_id')
        	->where($map)
        	->limit($Page->firstRow.','.$Page->listRows)
        	->select();
        $term_data = M('terms_video')->where("term_id=$term_id")->field('term_id, name')->find();	
        $this->assign('term_data',$term_data);// 赋值数据集
        $this->assign('data',$data);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(':video_list'); // 输出模板
    }

    /**
	*	视频详情
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function info(){
    	$post_id = I('get.post_id', 0, 'int');
    	$post_hits = M('posts_video')->where("id=$post_id")->field('post_hits')->find();
    	$post_hits['post_hits'] ++ ;
    	$res = M('posts_video')->where("id=$post_id")->save($post_hits);
    	$data = M('posts_video')->where("id=$post_id")->find();
    	$comment_data = M('comments_comments')->where("post_id=$post_id")->order('path')->select();
    	$collect['post_id'] = $post_id;
        $collect['uid'] = $_SESSION['user_id'];
        $user_collect = M('comments_collect')->where($collect)->field('id')->find();
        $user_hit = M('comments_hit')->where($collect)->field('id')->find();
    	$this->assign($data);
    	$this->assign('user_collect', $user_collect);
    	$this->assign('user_hit', $user_collect);
    	$this->assign('comment_data', $comment_data);
        $this->display(':video_info');
    }

    /**
    *   视频评论
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function comment(){
        $post_id = I('get.post_id', 0, 'int');
        $user_id = I('get.user_id', 0, 'int');
        if ($user_id) {
            $last_id = M('comments_comments')->add($_POST);
            if ($last_id) {
                $data['url'] = U('video/info', array('post_id'=>$post_id));
                $data['post_id'] = $post_id;
                $data['uid'] = $user_id;
                $user_data = M('users')->where("id = $user_id")->field('user_nicename, mobile, avatar')->find();
                $data['full_name'] = $user_data['user_nicename'];
                $data['user_mobile'] = $user_data['mobile'];
                $data['user_avatar'] = $user_data['avatar'];
                $data['createtime'] = date('Y-m-d H:i:s');
                $data['content'] = $_POST['content'];
                $data['path'] = '0-'.$last_id;
                $res = M('comments_comments')->where("id=$last_id")->save($data);
                if ($res) {
                    $this->redirect('video/info', array('post_id'=>$post_id));
                } else {
                    $this->error('评论失败');
                }
            } else {
                $this->error('评论失败');
            }
        } else {
            $this->error('请先登录');
        }
    }

    /**
    *   视频回复
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function comment_reply(){
        $post_id = I('get.post_id', 0, 'int');
        $user_id = I('get.user_id', 0, 'int');
        if ($user_id) {
            $last_id = M('comments_comments')->add($_POST);
            if ($last_id) {
                $data['url'] = U('video/info', array('post_id'=>$post_id));
                $data['post_id'] = $post_id;
                $data['uid'] = $user_id;
                $user_data = M('users')->where("id = $user_id")->field('user_nicename, mobile, avatar')->find();
                $data['full_name'] = $user_data['user_nicename'];
                $data['user_mobile'] = $user_data['mobile'];
                $data['user_avatar'] = $user_data['avatar'];
                $data['createtime'] = date('Y-m-d H:i:s');
                $data['content'] = $_POST['content'];
                $data['parentid'] = $_GET['comment_id'];
                $data['path'] = '0-'.$_GET['comment_id'].'-'.$last_id;
                $res = M('comments_comments')->where("id=$last_id")->save($data);
                if ($res) {
                    $this->redirect('video/info', array('post_id'=>$post_id));
                } else {
                    $this->error('回复失败');
                }
            } else {
                $this->error('回复失败');
            }
        } else {
            $this->error('请先登录');
        }
    }

    /**
    *   视频收藏
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function collect(){
        $post_id = I('get.post_id', 0, 'int');
        $user_id = I('get.user_id', 0, 'int');
        $collect['post_id'] = $post_id;
        $collect['uid'] = $user_id;
		$result = M('comments_collect')->where($collect)->field('id')->find();
		if ($result) {
			$this->error('您已经收藏过啦');
		}
        if ($user_id) {
            $data['url'] = U('video/info', array('post_id'=>$post_id));
            $data['post_id'] = $post_id;
            $data['uid'] = $user_id;
            $user_data = M('users')->where("id = $user_id")->field('user_nicename, mobile, avatar')->find();
            $data['full_name'] = $user_data['user_nicename'];
            $data['user_mobile'] = $user_data['mobile'];
            $data['user_avatar'] = $user_data['avatar'];
            $data['createtime'] = date('Y-m-d H:i:s');
            $res = M('comments_collect')->where("id=$last_id")->add($data);
            if ($res) {
                $this->redirect('video/info', array('post_id'=>$post_id));
            } else {
                $this->error('收藏失败');
            }
            $this->error('请先登录');
        }
    }

    /**
    *   视频点赞
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function hit(){
        $post_id = I('get.post_id', 0, 'int');
        $user_id = I('get.user_id', 0, 'int');
        $collect['post_id'] = $post_id;
        $collect['uid'] = $user_id;
		$result = M('comments_hit')->where($collect)->field('id')->find();
		if ($result) {
			$this->error('您已经赞过啦');
		}
        if ($user_id) {
            $data['url'] = U('video/info', array('post_id'=>$post_id));
            $data['post_id'] = $post_id;
            $data['uid'] = $user_id;
            $user_data = M('users')->where("id = $user_id")->field('user_nicename, mobile, avatar')->find();
            $data['full_name'] = $user_data['user_nicename'];
            $data['user_mobile'] = $user_data['mobile'];
            $data['user_avatar'] = $user_data['avatar'];
            $data['createtime'] = date('Y-m-d H:i:s');
            $res = M('comments_hit')->where("id=$last_id")->add($data);
            if ($res) {
                $this->redirect('video/info', array('post_id'=>$post_id));
            } else {
                $this->error('点赞失败');
            }
            $this->error('请先登录');
        }
    }



}