<?php
namespace Admin\Controller;
use Think\Controller;
class LoginController extends Controller {

    /** 
	 * 后台登录 
	 * author 琯琯 
	 * date 2017.4.30
	 */
    public function login() {
        $admin_id=session('admin_user_id');
    	if(!empty($admin_id)){//已经登录
    		redirect('index/index');
    	}else{
    		$this->display(":login");
    	}
    }

    /** 
	 * 验证码生成 
	 * author 琯琯 
	 * date 2017.4.30
	 */  
	public function verify_code(){  
	    $Verify = new \Think\Verify();  
	    $Verify->expire = 30;  
	    $Verify->fontSize = 18;  
	    $Verify->length   = 4;  
	    $Verify->useNoise = false;  
	    //$Verify->codeSet = '0123456789';  
	    $Verify->imageW = 296;  
	    $Verify->imageH = 50;
	    //$Verify->useImgBg = true;   
	    $Verify->bg = array(243, 251, 254);   
	    //$Verify->expire = 600;  
	    $Verify->entry();  
	}  
    
	/** 
	 * 验证码检查 
	 * author 琯琯 
	 * date 2017.4.30
	 */ 
	private function check_verify($code, $id = ""){  
	    $verify = new \Think\Verify();  
	    return $verify->check($code, $id);  
	}  

	/** 
	 * 退出 
	 * author 琯琯 
	 * date 2017.4.30
	 */ 
    public function logout(){
    	session('admin_user_id',null); 
    	redirect('login/login');
    }
    
    /** 
	 * 执行登录
	 * author 琯琯 
	 * date 2017.4.30
	 */
    public function dologin(){
    	$name = I("post.user_nicename");
    	if(empty($name)){
    		$this->error('账号不能为空');
    	}
    	$pass = I("post.user_pass");
    	if(empty($pass)){
    		$this->error('密码不能为空');
    	}
    	// $verrify = I("post.code");
    	// if(empty($verrify)){
    	// 	$this->error('验证码不能为空');
    	// }
    	// 验证码
    	// if (!$this->check_verify($verrify) ) {
    	// 	$this->error('验证码不对');
    	// }
    	$user_m = M('users');
    	$data = $user_m->where("user_nicename='$name'")->find();
    	// 用户
    	if (!$data) {
    		$this->error('该用户不存在');
    	}
    	// 状态
    	if (!$data['user_status']) {
    		$this->error('该用户被禁用');
    	}
    	// 管理员
    	if ($data['user_type'] == 3) {
    		$this->error('该用户不是管理员');
    	}
    	// 加密
    	$pass = md5($pass);
    	$result = $user_m->where("user_nicename='$name' and user_pass='$pass'")->find();
    	if ($result) {
    		//登入成功页面跳转
			session('admin_user_id',$result["id"]);
			session('admin_user_nicename',$result["user_nicename"]);
			$result['last_login_ip']=get_client_ip(0,true);
			$result['last_login_time']=date("Y-m-d H:i:s");
			$user_m->save($result);
			cookie("admin_user_nicename",$name,3600*24*30);
			$this->redirect('index/index');
    	} else {
    		$this->error('密码错误');
    	}
    }

}