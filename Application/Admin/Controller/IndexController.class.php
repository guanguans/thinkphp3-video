<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {
	/**
	*	后台主页
	*	auth 琯琯
	*	date 2017.4.30
	*/
    public function index(){
    	if (!$_SESSION['admin_user_id']) {
    		$this->redirect('login/login');
    	}
    	$system_info = $this->system_info();
    	$this->assign('system_info', $system_info);
        $this->display(':index');
    }

    /**
	*	系统信息
	*	auth 琯琯
	*	date 2017.4.30
	*/
    private function system_info(){
    	$mysql= M()->query("select VERSION() as version");
    	$mysql=$mysql[0]['version'];
    	$mysql=empty($mysql)?L('UNKNOWN'):$mysql;
    	//server infomaions
    	$info = array(
			'操作系统' => PHP_OS,
			'运行环境' => $_SERVER["SERVER_SOFTWARE"],
	        'PHP版本' => PHP_VERSION,
	        'PHP版本' => phpversion(),
			'PHP运行方式' => php_sapi_name(),
			'MYSQL版本' =>$mysql,
			'个人空间' =>  "[<a href='https://user.qzone.qq.com/53222411?ptlang=2052&source=friendlist' target='_blank'>琯琯</a>]",
			'上传附件限制' => ini_get('upload_max_filesize'),
			'执行时间限制' => ini_get('max_execution_time') . "s",
			'剩余空间' => round((@disk_free_space(".") / (1024 * 1024)), 2) . 'M',
    	);
    	return $info;
    	exit;
    }

}