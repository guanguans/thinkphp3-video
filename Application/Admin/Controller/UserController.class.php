<?php
namespace Admin\Controller;
use Think\Controller;
class UserController extends Controller {
	/**
	*	用户列表
	*	ahthor 琯琯
	*	date 2017.4.29
	*/
    public function index(){
        $users = M('users'); // 实例化User对象
        // 条件
        $map['id'] = array('LIKE', '%'.I('id').'%');
        $pattern = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        if (is_numeric(I('name_mobile_email'))) {
        	$map['mobile'] = array('LIKE', '%'.I('name_mobile_email').'%');
        } else if (preg_match($pattern, I('name_mobile_email') ) ) {
        	$map['user_email'] = array('LIKE', '%'.I('name_mobile_email').'%');
        } else {
        	$map['user_nicename'] = array('LIKE', '%'.I('name_mobile_email').'%');
        }

        $count = $users->where($map)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count, 3);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出

        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $users->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign('data',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }

    /**
	*	用户添加
	*	ahthor 琯琯
	*	date 2017.4.29
	*/
    public function add(){
        $this->display();
    }

    /**
	*	用户执行添加
	*	ahthor 琯琯
	*	date 2017.4.29
	*/
    public function doadd(){
    	$user_pass = I('post.user_pass');
    	$user_pass2 = I('post.user_pass2');
    	if ($user_pass != $user_pass2) {
    		$this->error('两次密码不一样');
    	}
    	$data = array(
    		'user_nicename' => I('post.user_nicename'),	 
    		'user_pass' => md5(I('post.user_pass')),	 
    		'user_type' => I('post.user_type'),	 
    		'create_time' => date('Y-m-d H:i:s'),	 
    	);
    	$res = M('users')->add($data);
		if ($res) {
			$this->redirect('index', '添加成功');
		} else {
			$this->error('添加失败');
		}    	
    }

    /**
	*	用户删除
	*	ahthor 琯琯
	*	date 2017.4.29
	*/
    public function delete(){
    	$id = I('get.id');
    	if ($id == 8) {
    		$this->success('超级管理员禁止删除！');
    	} else {
    		$res = M('users')->where("id=$id")->delete();
	        if ($res) {
	        	$this->redirect('index');
	        } else {
	        	$this->error('删除失败');
        	}
    	}
    }

    /**
    *   用户编辑
    *   ahthor 琯琯
    *   date 2017.4.29
    */
    public function edit(){
        $id = I('get.id');
        $data = M('users')->where("id=$id")->find();
        if ($data) {
            // 若是超级用户
            if ($id == 8) {
                $this->assign($data);
                $this->display('supper_edit');
            } else {
                $this->assign($data);
                $this->display();
            }
        } else {
            $this->redirect('index');
        }   
    }

    /**
    *   用户执行编辑
    *   ahthor 琯琯
    *   date 2017.4.29
    */
    public function doedit(){
       $id = I('get.id');
       $data = I('post.');
       $data['user_pass'] = md5($data['user_pass']);
       $res = M('users')->where("id=$id")->save($data);
       if ($res) {
            $this->redirect('index');
       } else {
            $this->error('修改失败');
       }
    }

    /**
    *   用户执行个人编辑
    *   ahthor 琯琯
    *   date 2017.4.29
    */
    public function supper_doedit(){
       $id = I('get.id');
       $user_pass_old = md5(I('post.user_pass_old'));
       $result = M('users')->where("id=$id and user_pass='$user_pass_old'")->field('id')->find();
       if ($result) {
            $data = I('post.');
            $data['user_pass'] = md5($data['user_pass']);
            unset($data['user_pass_old']);
            $res = M('users')->where("id=$id")->save($data);
            if ($res) {
                $this->redirect('index');
            } else {
                $this->error('修改失败');
            }
        } else {
            $this->error('旧密码错误');
        }
       
    }

    /**
    *   用户状态
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function status(){
        if (IS_GET) {
            $id = I('get.id');
            if ($id == 8 ) {
            	$this->error('超级管理员禁止拉黑！');
            }
            $user_status = I('get.user_status');
            if ($user_status) {
                $data['user_status'] = 0;
            } else {
                $data['user_status'] = 1;
            }
            $res = M('users')->where("id=$id")->save($data);
            if ($res) {
                // $this->success('删除成功', 'class_index');
                $this->redirect('index');
            } else {
                $this->error('改变失败');
            }
        }
    }
    
}