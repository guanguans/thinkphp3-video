<?php
namespace Admin\Controller;
use Think\Controller;
// use Think\Org\Page;
class SlideController extends  Controller {
	/**
	*	轮播图列表
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function index(){
        $slide = M('slide'); // 实例化User对象
        // 条件
        $map['slide_name'] = array('LIKE', '%'.I('slide_name').'%');
        if (!empty(I('slide_cid')) && I('slide_cid') !== 'all') {
             $map['slide_cid'] = I('slide_cid');
        }
        $count = $slide->where($map)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,3);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $slide->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
        $class = M('slide_cat')->field('cid,cat_name')->select();
        $this->assign('class',$class);// 分类
        $this->assign('data',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }

    /**
    *   轮播图添加
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function add(){
        $data = M('slide_cat')->field('cid,cat_name')->select();
        $this->assign('data',$data);
        $this->display();
    }

    /**
    *   轮播图执行添加
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function doadd(){
        if (IS_POST) {
            $slide_pic = $this->upload_one($_FILES['slide_pic']);
            if (!$slide_pic) {
                $this->error('添加失败');
            } 
            $data = I('post.');
            $data['slide_pic'] = $slide_pic['savepath'].$slide_pic['savename'];
            $res = M('slide')->add($data);
            if ($res) {
                // $this->redirect('index');
                $this->success('添加成功', 'index');
            } else {
                $this->error('添加失败');
            }
        }
    }

    /**
    *   轮播图删除
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function delete(){
        if (IS_GET) {
            $slide_id = I('get.slide_id');
            $res = M('slide')->delete($slide_id);
            if ($res) {
                // $this->success('删除成功', 'index');
                $this->redirect('index');
            } else {
                $this->error('删除失败');
            }
        }
    }

    /**
    *   轮播图编辑
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function edit(){

        if (IS_GET) {
            $slide_id = I('get.slide_id');
            $data = M('slide')->where("slide_id=$slide_id")->find();
            $class = M('slide_cat')->field('cid,cat_name')->select();
            if ($data) {
                $this->assign($data);
                $this->assign('class', $class);
                $this->display();
            } else {
                $this->redirect('index');
            }
        }
    }

    /**
    *   轮播图执行编辑
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function doedit(){
        if (IS_POST) {
            $slide_pic = $this->upload_one($_FILES['slide_pic']);
            if (!$slide_pic) {
                $this->error('修改失败');
            } 
            $slide_id = I('get.slide_id');
            $data = I('post.');
            $data['slide_pic'] = $slide_pic['savepath'].$slide_pic['savename'];
            $res = M('slide')->where("slide_id=$slide_id")->save($data);
            if ($res) {
                $this->redirect('index');
            } else {
                $this->error('修改失败');
            }
        }
    }

    /**
    *   单个文件上传
    * @param array $files    上传文件信息  $_FILES['slide_pic']
    * @param int $maxSize    上传大小限制  单位kb 默认 1*1024*1024
    * @param str $rootPath   文件保存根目录       默认 './Public/common/images/'
    * @param array $exts     上传后缀限制         默认 array('jpg', 'gif', 'png', 'jpeg')
    * @param Boolean  $autoSub  自动使用子目录保存上传文件 默认 true
    *   author by 琯琯
    *   date 2017.4.28
    */
    private function upload_one($files, $maxSize, $rootPath, $exts){
        $config = array(
            'maxSize'    =>    1*1024*1024,    
            'rootPath'   =>    './Public/common/images/', // 设置附件上传根目录 
            'saveName'   =>    array('uniqid',''),//上传文件的保存规则    
            'exts'       =>    array('jpg', 'gif', 'png', 'jpeg'),    
            'autoSub'    =>    true,//自动使用子目录保存上传文件    
            'subName'    =>    array('date','Ymd'),
        );

        if (!empty($maxSize)) {
            $config['maxSize'] = $maxSize;
        }

        if (!empty($rootPath)) {
            $config['rootPath'] = $rootPath;
        }

        if (!empty($exts)) {
            $config['exts'] = $exts;
        }

        $upload = new \Think\Upload($config);// 实例化上传类
        $info   =   $upload->uploadOne($files);

        if(!$info) {
            // return $upload->getError();
            return false;
        }else{      
            return $info;    
        }
    }
    
    /**
    *   轮播图状态改变
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function status(){
        if (IS_GET) {
            $slide_id = I('get.slide_id');
            $slide_status = I('get.slide_status');
            if ($slide_status) {
                $data['slide_status'] = 0;
            } else {
                $data['slide_status'] = 1;
            }
            $res = M('slide')->where("slide_id=$slide_id")->save($data);
            if ($res) {
                // $this->success('删除成功', 'class_index');
                $this->redirect('index');
            } else {
                $this->error('改变失败');
            }
        }
    }

    /**
	*	轮播图分类列表搜索分页
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function class_index(){
        $slide_cat = M('slide_cat'); // 实例化User对象
        $map['cat_name'] = array('LIKE', '%'.I('cat_name').'%');
        $map['cat_status'] = 1;
        $count = $slide_cat->where($map)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,2);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $slide_cat->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign('data',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }

    /**
	*	轮播图分类列表添加
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function class_add(){
        $this->display();
    }

    /**
	*	轮播图分类添加
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function class_doadd(){
    	if (IS_POST) {
    		$data = I('post.');
    		$res = M('slide_cat')->add($data);
    		if ($res) {
    			$this->success('添加成功', 'class_index');
    			// $this->redirect('class_index');
    		} else {
    			$this->error('添加失败');
    		}
    	}
    }

    /**
	*	轮播图分类列表删除
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function class_delete(){
    	if (IS_GET) {
    		$cid = I('get.cid');
            $res_slide = M("slide")->where("slide_cid=$cid")->field('slide_id')->select();
            if ($res_slide) {
                $this->error('请先删除该分类下的轮播图');
            }
    		$res = M('slide_cat')->delete($cid);
    		if ($res) {
    			// $this->success('删除成功', 'class_index');
    			$this->redirect('class_index');
    		} else {
    			$this->error('删除失败');
    		}
    	}
    }

    /**
	*	轮播图分类列表编辑
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function class_edit(){
    	if (IS_GET) {
    		$cid = I('get.cid');
    		$data = M('slide_cat')->where("cid=$cid")->find();
    		if ($data) {
    			$this->assign($data);
    			$this->display();
    		} else {
    			$this->redirect('class_index');
    		}
    	}
    }

    /**
	*	轮播图分类列表执行编辑
	*	author by 琯琯
	*	date 2017.4.28
	*/
    public function class_doedit(){
    	if (IS_POST) {
    		$cid = I('get.cid');
    		$data = I('post.');
    		$data = M('slide_cat')->where("cid=$cid")->save($data);
    		if ($data) {
    			$this->redirect('class_index');
    		} else {
    			$this->error('修改失败');
    		}
    	}
    }


}