<?php
namespace Admin\Controller;
use Think\Controller;
class VideoController extends Controller {

	/**
	* 视频列表
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function index(){
        $posts_video = M('posts_video'); // 实例化User对象
        // 条件
        $map['post_title'] = array('LIKE', '%'.I('post_title').'%'); //标题
        $map['post_status'] = array('neq', 3); //标题
        if (!empty(I('post_parent')) && I('post_parent') !== 'all') { //分类
             $map['post_parent'] = I('post_parent');
        }
        //$map['post_date'] = array(array('gt',I('post_date')),array('lt',I('post_date')));
        $count = $posts_video->where($map)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $posts_video->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
        $class = M('terms_video')->field('term_id, name, path, parent')->select();
        $this->assign('data',$list);// 赋值数据集
        $this->assign('list',$class);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }

    /**
	* 视频添加
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function add(){
    	$data = M('terms_video')->field('term_id,name,path,parent')->order('path')->select();
    	$this->assign('data', $data);
        $this->display();
    }

    /**
	* 视频执行添加
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function doadd(){
        if (IS_POST) {
            $data = I('post.');
            // 视频
            if ($_FILES['video']['tmp_name']) {
                $post_one_video = $this->upload_video($_FILES['video']);
                if ($post_one_video) {
                    $data['video'] = $post_one_video['savepath'].$post_one_video['savename'];
                } else {
                    $this->error('添加失败');
                }
            }
            // 图片
            if ($_FILES['post_one_image']['tmp_name']) {
                $post_one_image = $this->upload_one($_FILES['post_one_image']);
                if ($post_one_image) {
                    $data['post_one_image'] = $post_one_image['savepath'].$post_one_image['savename'];
                } else {
                    $this->error('添加失败');
                }
            }
            $term_id = $data['term_id'];
            unset($data['term_id']);
            $data['post_content'] = htmlspecialchars_decode($data['post_content']);
            $data['post_modified'] = date('Y-m-d H:i:s');
            $data['post_parent'] = $term_id;
            $last_id = M('posts_video')->add($data);
            if ($last_id) {
                $post_data['object_id'] = $last_id;
                $post_data['term_id'] = $term_id;
                $result = M('term_relationships_video')->add($post_data);
                if ($result) {
                    $this->success('添加成功', 'index');
                } else {
                     $this->error('添加失败');
                }
            } else {
                $this->error('添加失败');
            }
        } else {
            $this->error('非法操作');
        }
    }

    /**
    *   单个文件上传
    * @param array $files    上传文件信息  $_FILES['slide_pic']
    * @param int $maxSize    上传大小限制  单位kb 默认 1*1024*1024
    * @param str $rootPath   文件保存根目录       默认 './Public/common/images/'
    * @param array $exts     上传后缀限制         默认 array('jpg', 'gif', 'png', 'jpeg')
    * @param Boolean  $autoSub  自动使用子目录保存上传文件 默认 true
    * author by 琯琯
    * date 2017.4.28  上传图片
    */
    private function upload_one($files, $maxSize, $rootPath, $exts){
        $config = array(
            'maxSize'    =>    3*1024*1024,    
            'rootPath'   =>    './Public/common/images/', // 设置附件上传根目录 
            'saveName'   =>    array('uniqid',''),//上传文件的保存规则    
            'exts'       =>    array('jpg', 'gif', 'png', 'jpeg'),    
            'autoSub'    =>    true,//自动使用子目录保存上传文件    
            'subName'    =>    array('date','Ymd'),
        );

        if (!empty($maxSize)) {
            $config['maxSize'] = $maxSize;
        }

        if (!empty($rootPath)) {
            $config['rootPath'] = $rootPath;
        }

        if (!empty($exts)) {
            $config['exts'] = $exts;
        }

        $upload = new \Think\Upload($config);// 实例化上传类
        $info   =   $upload->uploadOne($files);

        if(!$info) {
            // return $upload->getError();
            return false;
        }else{      
            return $info;    
        }
    }

    /**
    *   单个文件上传
    * @param array $files    上传文件信息  $_FILES['slide_pic']
    * @param int $maxSize    上传大小限制  单位kb 默认 1*1024*1024
    * @param str $rootPath   文件保存根目录       默认 './Public/common/images/'
    * @param array $exts     上传后缀限制         默认 array('jpg', 'gif', 'png', 'jpeg')
    * @param Boolean  $autoSub  自动使用子目录保存上传文件 默认 true
    * author by 琯琯
    * date 2017.4.28 上传视频
    */
    private function upload_video($files, $maxSize, $rootPath, $exts){
        $config = array(
            'maxSize'    =>    20*1024*1024,    
            'rootPath'   =>    './Public/common/video/', // 设置附件上传根目录 
            'saveName'   =>    array('uniqid',''),//上传文件的保存规则    
            'exts'       =>    array('mp4', 'avi', 'wmv', 'rmvb', 'mkv'),
            'autoSub'    =>    true,//自动使用子目录保存上传文件    
            'subName'    =>    array('date','Ymd'),
        );

        if (!empty($maxSize)) {
            $config['maxSize'] = $maxSize;
        }

        if (!empty($rootPath)) {
            $config['rootPath'] = $rootPath;
        }

        if (!empty($exts)) {
            $config['exts'] = $exts;
        }

        $upload = new \Think\Upload($config);// 实例化上传类
        $info   =   $upload->uploadOne($files);

        if(!$info) {
            //return $upload->getError();
            return false;
        }else{      
            return $info;    
        }
    }

    /**
	* 视频删除状态为删除
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function delete(){
        $id = I('get.id', 0, 'int');
        $res = M('posts_video')->where("id=$id")->save(array('post_status'=>3) );
        if ($res) {
                $this->redirect('index');
        } else {
            $this->error('删除失败');
        }
    }

    /**
    * 视频回收站列表
    * ahthor 琯琯
    * date 2017.4.30
    */
    public function recycle_index(){
        $posts_video = M('posts_video'); // 实例化User对象
        // 条件
        $map['post_title'] = array('LIKE', '%'.I('post_title').'%'); //标题
        $map['post_status'] = array('eq', 3); //标题
        if (!empty(I('post_parent')) && I('post_parent') !== 'all') { //分类
             $map['post_parent'] = I('post_parent');
        }
        //$map['post_date'] = array(array('gt',I('post_date')),array('lt',I('post_date')));
        $count = $posts_video->where($map)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $posts_video->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
        $class = M('terms_video')->field('term_id, name, path, parent')->select();
        $this->assign('data',$list);// 赋值数据集
        $this->assign('list',$class);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }

    /**
    * 视频回收站删除
    * ahthor 琯琯
    * date 2017.4.30
    */
    public function recycle_delete(){
        $id = I('get.id', 0, 'int');
        $res = M('posts_video')->where("id=$id")->delete();
        if ($res) {
            $result = M('term_relationships_video')->where("object_id=$id")->delete();
            if ($result) {
                $this->redirect('index');
            } else {    
                $this->error('删除失败');
            }
        } else {
            $this->error('删除失败');
        }
    }

    /**
    * 视频回收站还原
    * ahthor 琯琯
    * date 2017.4.30
    */
    public function recycle_recover(){
        $id = I('get.id', 0, 'int');
        $res = M('posts_video')->where("id=$id")->save(array('post_status'=>0) );
        if ($res) {
                $this->success('还原成功');
        } else {
            $this->error('还原失败');
        }
    }

    /**
	* 视频修改
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function edit(){
        $id = I('get.id', 0, 'int');
        $data = M('posts_video')->where("id=$id")->find();
        $list = M('terms_video')->field('term_id,name,path,parent')->order('path')->select();
        $this->assign($data);
        $this->assign('data', $list);
        $this->display();
    }

    /**
	* 视频执行修改
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function doedit(){
        $id = I('get.id', 0, 'int');
        $data = I('post.');
        // 视频
        if ($_FILES['video_new']['tmp_name']) {
            $post_one_video = $this->upload_video($_FILES['video_new']);
            if ($post_one_video) {
                $data['video'] = $post_one_video['savepath'].$post_one_video['savename'];
            } else {
                $this->error('修改失败');
            }
        }
        if ($_FILES['post_one_image']['tmp_name']) {
            $post_one_image = $this->upload_one($_FILES['post_one_image']);
            if ($post_one_image) {
                $data['post_one_image'] = $post_one_image['savepath'].$post_one_image['savename'];
            } else {
                $this->error('修改失败');
            }
        }

        $term_id['term_id'] = $data['post_parent'];
        $res = M('term_relationships_video')->where("object_id=$id")->save($term_id);
        $data['post_content'] = htmlspecialchars_decode($data['post_content']);
        $data['post_modified'] = date('Y-m-d H:i:s');
        $result = M('posts_video')->where("id=$id")->save($data);
        if ($result) {
            $this->success('修改成功');
        } else {
            $this->error('修改失败');
        }
    }

    /**
    * 视频状态
    * ahthor 琯琯
    * date 2017.4.30
    */
    public function post_status(){
        $id = I('get.id', 0, 'int' );
        $post_status = I('get.post_status', 0, 'int' );
        if ($post_status) {
            $data['post_status'] = 0;
        } else {
            $data['post_status'] = 1;
        }
        $res = M('posts_video')->where("id=$id")->save($data);
        if ($res) {
            $this->redirect('index');
        } else {
            $this->error('操作失败');
        }
    }

    /**
    * 视频置顶
    * ahthor 琯琯
    * date 2017.4.30
    */
    public function istop(){
        $id = I('get.id', 0, 'int' );
        $istop = I('get.istop', 0, 'int' );
        if ($istop) {
            $data['istop'] = 0;
        } else {
            $data['istop'] = 1;
        }
        $res = M('posts_video')->where("id=$id")->save($data);
        if ($res) {
            $this->redirect('index');
        } else {
            $this->error('操作失败');
        }
    }

    /**
    * 视频推荐
    * ahthor 琯琯
    * date 2017.4.30
    */
    public function recommended(){
        $id = I('get.id', 0, 'int' );
        $recommended = I('get.recommended', 0, 'int' );
        if ($recommended) {
            $data['recommended'] = 0;
        } else {
            $data['recommended'] = 1;
        }
        $res = M('posts_video')->where("id=$id")->save($data);
        if ($res) {
            $this->redirect('index');
        } else {
            $this->error('操作失败');
        }
    }

    /**
	* 视频分类列表
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function class_index(){
    	$data = M('terms_video')->field('term_id,name,path,description,parent')->order('path')->select();
    	$this->assign('data', $data);
        $this->display();
    }

    /**
	* 视频分类添加
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function class_add(){
    	$data = M('terms_video')->field('term_id,name,path,parent')->order('path')->select();
    	$this->assign('data', $data);
        $this->display();
    }

    /**
	* 视频分类执行添加
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function class_doadd(){
    	if (IS_POST) {
    		$term_id = $_POST['parent'];
			$last_id =  M('terms_video')->add($_POST);
			if ($last_id) {
				if ($term_id == 'one') {
					$data['parent'] = 0;
					$data['path'] = '0-'.$last_id;
				} else {
					$path = M('terms_video')->where("term_id='$term_id'")->field('path')->find();
					$data['parent'] = $term_id;
					$data['path'] = $path['path'].'-'.$last_id;
				}
				$result = M('terms_video')->where("term_id=$last_id")->save($data);
				if ($result) {
					$this->redirect('class_index');
				} else {
					$this->error('添加失败');
				}
			} else {
				$this->error('添加失败');
			}
    	}
    }

    /**
	* 视频分类删除
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function class_delete(){
    	$term_id = I('term_id');
    	$res = M('terms_video')->where("parent='$term_id'")->field('term_id')->find();
    	if (!$res) {
    		$data = M('terms_video')->where("term_id='$term_id'")->delete();
        	$this->redirect('class_index');
    	} else {
    		$this->error('请先删除子分类');
    	}
    	
    }

    /**
	* 视频分类修改
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function class_edit(){
    	$term_id = I('term_id');
    	$data = M('terms_video')->where("term_id='$term_id'")->field('term_id,name,description')->find();
    	$this->assign($data);
        $this->display();
    }

    /**
	* 视频分类执行修改
	* ahthor 琯琯
	* date 2017.4.30
	*/
    public function class_doedit(){
    	$term_id = I('get.term_id');
    	$res = M('terms_video')->where("term_id=$term_id")->save($_POST);
    	if ($res) {
    		$this->redirect('class_index');
    	} else {
    		$this->error('修改失败');
    	}
    }

}