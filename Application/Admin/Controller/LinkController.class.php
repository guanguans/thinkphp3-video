<?php
namespace Admin\Controller;
use Think\Controller;
class LinkController extends Controller {

	/**
	* 友情链接列表
	* auth 琯琯
	* date 2016.4.29
	*/
    public function index(){
        $links = M('links'); // 实例化User对象
        // 条件
        $map['link_name'] = array('LIKE', '%'.I('link_name').'%');
        $count = $links->where($map)->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,3);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $links->where($map)->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign('data',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }

    /**
	* 友情链接添加
	* auth 琯琯
	* date 2016.4.29
	*/
    public function add(){
        $this->display();
    }

    /**
	* 友情链接执行添加
	* auth 琯琯
	* date 2016.4.29
	*/
    public function doadd(){
    	if (IS_POST) {
            $link_image = $this->upload_one($_FILES['link_image']);
            $data = I('post.');
            if ($link_image) {
            	$data['link_image'] = $link_image['savepath'].$link_image['savename'];
            }
            $res = M('links')->add($data);
            if ($res) {
                // $this->redirect('index');
                $this->success('添加成功', 'index');
            } else {
                $this->error('添加失败');
            }
        }
    }

    /**
    *   友情链接删除
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function delete(){
        if (IS_GET) {
            $link_id = I('get.link_id');
            $res = M('links')->delete($link_id);
            if ($res) {
                // $this->success('删除成功', 'index');
                $this->redirect('index');
            } else {
                $this->error('删除失败');
            }
        }
    }

    /**
    *   友情链接编辑
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function edit(){
        if (IS_GET) {
            $link_id = I('get.link_id');
            $data = M('links')->where("link_id=$link_id")->find();
            if ($data) {
                $this->assign($data);
                $this->display();
            } else {
                $this->redirect('index');
            }
        }
    }
    
    /**
    *   友情链接执行编辑
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function doedit(){
        if (IS_POST) {
            $link_image = $this->upload_one($_FILES['link_image']);
            $link_id = I('get.link_id');
            $data = I('post.');
            $data['link_image'] = $link_image['savepath'].$link_image['savename'];
            $res = M('links')->where("link_id=$link_id")->save($data);
            if ($res) {
                $this->redirect('index', '修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }

    /**
    *   友情链接状态改变
    *   author by 琯琯
    *   date 2017.4.28
    */
    public function status(){
        if (IS_GET) {
            $link_id = I('get.link_id');
            $link_status = I('get.link_status');
            if ($link_status) {
                $data['link_status'] = 0;
            } else {
                $data['link_status'] = 1;
            }
            $res = M('links')->where("link_id=$link_id")->save($data);
            if ($res) {
                // $this->success('删除成功', 'class_index');
                $this->redirect('index');
            } else {
                $this->error('改变失败');
            }
        }
    }

    /**
    *   单个文件上传
    * @param array $files    上传文件信息  $_FILES['slide_pic']
    * @param int $maxSize    上传大小限制  单位kb 默认 1*1024*1024
    * @param str $rootPath   文件保存根目录       默认 './Public/common/images/'
    * @param array $exts     上传后缀限制         默认 array('jpg', 'gif', 'png', 'jpeg')
    * @param Boolean  $autoSub  自动使用子目录保存上传文件 默认 true
    *   author by 琯琯
    *   date 2017.4.28
    */
    private function upload_one($files, $maxSize, $rootPath, $exts){
        $config = array(
            'maxSize'    =>    1*1024*1024,    
            'rootPath'   =>    './Public/common/images/', // 设置附件上传根目录 
            'saveName'   =>    array('uniqid',''),//上传文件的保存规则    
            'exts'       =>    array('jpg', 'gif', 'png', 'jpeg'),    
            'autoSub'    =>    true,//自动使用子目录保存上传文件    
            'subName'    =>    array('date','Ymd'),
        );

        if (!empty($maxSize)) {
            $config['maxSize'] = $maxSize;
        }

        if (!empty($rootPath)) {
            $config['rootPath'] = $rootPath;
        }

        if (!empty($exts)) {
            $config['exts'] = $exts;
        }

        $upload = new \Think\Upload($config);// 实例化上传类
        $info   =   $upload->uploadOne($files);

        if(!$info) {
            // return $upload->getError();
            return false;
        }else{      
            return $info;    
        }
    }
}