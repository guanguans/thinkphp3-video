<?php
namespace Admin\Controller;
use Think\Controller;
class SettingController extends Controller {
	/**
	* 网站配置
	* author 琯琯	
	* date 2017.4.30
	*/
    public function index(){
    	$data = M('options')->select();
    	foreach ($data as $key=> $value) {
    		$this->assign($value['option_name'], $value['option_value']);
    	}
        $this->display();
    }

    /**
	* 网站配置执行修改
	* author 琯琯	
	* date 2017.4.30
	*/
    public function doedit(){
    	$data = I('post.');
    	if (IS_POST) {
    		$res = M('options')->where('1')->delete();
    		foreach ($data as $key => $value) {
    			$list = array(
    					'option_name'=>$key,
    					'option_value'=> $value,
    				);
    			$result = M('options')->add($list);
    			if (!$result) {
    				$this->error('修改失败');
    			}
    		}
		    $this->success('修改成功');		
    	}
    	
    }

    /**
	* 网站配置清除
	* author 琯琯	
	* date 2017.4.30
	*/
    public function clear(){
    	// 删除所有数据
    	$res = M('options')->where('1')->delete();
    	if ($res ) {
			$this->redirect('setting/index');
    	} else {
    		$this->error('清空失败');
    	}
    }


}