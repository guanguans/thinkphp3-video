<?php

/**
 * 获取当前登录的管事员id
 * @return int
 */
function get_current_admin_id(){
	return session('admin_user_id');
}


/**
*   单个文件上传
* @param array $files    上传文件信息  $_FILES['slide_pic']
* @param int $maxSize    上传大小限制  单位kb 默认 1*1024*1024
* @param str $rootPath   文件保存根目录       默认 './Public/common/images/'
* @param array $exts     上传后缀限制         默认 array('jpg', 'gif', 'png', 'jpeg')
* @param Boolean  $autoSub  自动使用子目录保存上传文件 默认 true
*   author by 琯琯
*   date 2017.4.28
*/
function upload_one($files, $maxSize, $rootPath, $exts){
    $config = array(
        'maxSize'    =>    1*1024*1024,    
        'rootPath'   =>    './Public/common/images/', // 设置附件上传根目录 
        'saveName'   =>    array('uniqid',''),//上传文件的保存规则    
        'exts'       =>    array('jpg', 'gif', 'png', 'jpeg'),    
        'autoSub'    =>    true,//自动使用子目录保存上传文件    
        'subName'    =>    array('date','Ymd'),
    );

    if (!empty($maxSize)) {
        $config['maxSize'] = $maxSize;
    }

    if (!empty($rootPath)) {
        $config['rootPath'] = $rootPath;
    }

    if (!empty($exts)) {
        $config['exts'] = $exts;
    }

    $upload = new \Think\Upload($config);// 实例化上传类
    $info   =   $upload->uploadOne($files);

    if(!$info) {
        // return $upload->getError();
        return false;
    }else{      
        return $info;    
    }
}
