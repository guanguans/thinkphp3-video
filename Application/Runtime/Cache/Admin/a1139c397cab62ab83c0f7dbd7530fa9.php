<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
    <title>视频</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="/thinkphp3-demo/Public/admin/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="/thinkphp3-demo/Public/admin/css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="/thinkphp3-demo/Public/admin/css/lines.css" rel='stylesheet' type='text/css' />
<link href="/thinkphp3-demo/Public/admin/css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="/thinkphp3-demo/Public/admin/js/jquery.min.js"></script>
<!-- Nav CSS -->
<link href="/thinkphp3-demo/Public/admin/css/custom.css" rel="stylesheet">
<!-- Metis Menu Plugin JavaScript -->
<script src="/thinkphp3-demo/Public/admin/js/metisMenu.min.js"></script>
<script src="/thinkphp3-demo/Public/admin/js/custom.js"></script>
<!-- Graph JavaScript -->
<script src="/thinkphp3-demo/Public/admin/js/d3.v3.js"></script>
<script src="/thinkphp3-demo/Public/admin/js/rickshaw.js"></script>
<script src="/thinkphp3-demo/Public/admin/js/calendar/laydate.js"></script>
<style>
	#page-wrapper .top{margin-top: 0px;}
	#page-wrapper .color{background: #F7F7F9;color:#06D995;text-align: center;}
	#page-wrapper .color h2{font-size: 30px;line-height: 13px;}
	.nav-tabs a{color:#06D995;}
	#page{text-align: center;}
	#page a{padding:5px;}
	#page span{padding:5px;}
</style>


</head>
<body>
<div id="wrapper">
    <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo U('admin/index/index');?>">琯琯后台管理系统</a>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-nav navbar-right">
	    <li class="dropdown">
    		<a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><img src="/thinkphp3-demo/Public/common/images/headicon_30.png"><span class="badge"></span></a>
    		<ul class="dropdown-menu">
				<li class="dropdown-menu-header text-center">
					<strong><?php echo ($_SESSION['admin_user_nicename']); ?></strong>
				</li>
                <li class="m_2 text-center"><a href="<?php echo U('setting/index');?>"><i class="fa fa-cog"></i>网站配置</a></li>
                <li class="m_2 text-center"><a href="<?php echo U('user/edit', array('id'=>$_SESSION['admin_user_id']));?>"><i class="fa fa-user"></i>修改信息</a></li>
				<li class="m_2 text-center"><a href="<?php echo U('login/logout');?>"><i class="fa fa-sign-out"></i>退出</a></li>	
    		</ul>
  		</li>
	</ul>
	<!-- <form class="navbar-form navbar-right">
      <input type="text" class="form-control" value="搜索..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '搜索...';}">
    </form> -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="<?php echo U('admin/index/index');?>"><i class="fa fa-home fa-fw nav_icon"></i>后台首页</a>
                </li>
                <li>
                    <a href="/thinkphp3-demo" target="_blank"><i class="fa fa-home fa-fw nav_icon"></i>网站首页</a>
                </li>
                <li>
                    <a href="<?php echo U('setting/index');?>"><i class="fa fa-cogs fa-fw nav_icon"></i>网站配置</a>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-user fa-fw nav_icon"></i>用户管理<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo U('user/index');?>">本站用户</a>
                        </li>
                        <li>
                            <a href="#">第三方用户</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-video-camera fa-fw nav_icon"></i>视频管理<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                         <li>
                            <a href="<?php echo U('video/index');?>">视频</a>
                        </li>
                        <li>
                            <a href="<?php echo U('video/class_index');?>">分类</a>
                        </li>
                        <li>
                            <a href="<?php echo U('video/recycle_index');?>">回收站</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-list-alt fa-fw nav_icon"></i>新闻管理<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo U('news/index');?>">新闻</a>
                        </li>
                        <li>
                            <a href="<?php echo U('news/class_index');?>">分类</a>
                        </li>
                        <li>
                            <a href="<?php echo U('news/recycle_index');?>">回收站</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-comments fa-fw nav_icon"></i>评论管理<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Media</a>
                        </li>
                        <li>
                            <a href="#">Login</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-star fa-fw nav_icon"></i>收藏管理<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Media</a>
                        </li>
                        <li>
                            <a href="#">Login</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-plus-circle fa-fw nav_icon"></i>其他扩展<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="media.html"><i class="fa fa-spinner fa-fw nav_icon"></i>轮播图<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
		                        <li>
		                            <a href="<?php echo U('slide/index');?>">轮播图管理</a>
		                        </li>
                                 <li>
                                    <a href="<?php echo U('slide/class_index');?>">轮播图分类</a>
                                </li>
		                    </ul>
                        </li>
                        <li>
                            <a href="<?php echo U('link/index');?>" ><i class="fa fa-chain fa-fw nav_icon"></i>友情链接管理</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <div class="clearfix"></div>
    <!-- /.navbar-static-side -->
</nav>
    <div id="page-wrapper">
 		<div class="panel panel-success top" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
			<div class="panel-heading color">
				<h2>视频</h2>
				<div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
			</div>
			<ul class="nav nav-tabs">
				<li class="active"><a href="<?php echo U('video/index');?>">视频</a></li>
				<li><a href="<?php echo U('video/add');?>">视频添加</a></li>
				<li><a href="/thinkphp3-demo" target="_blank">前台首页</a></li>
				<li><a href="<?php echo U('index/index');?>">后台首页</a></li>
			</ul>
			<form class="well form-search" method="get" action="<?php echo U('video/index');?>">
				&nbsp;&nbsp;&nbsp;分类：
				<select  id="" name="post_parent" style="width:200px;">
				<option value="all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全部</option>
				<?php if(is_array($list)): foreach($list as $key=>$vo): if($vo['term_id'] == $post_parent): ?><option value="<?php echo ($vo["term_id"]); ?>" selected="selected">
						<?php $arr = explode('-', $vo['path'] ); for ($i=0; $i < count($arr); $i++) { echo '&nbsp;&nbsp;&nbsp;'; } ?>
				    	<?php if( strlen($vo['path']) != 3): ?>|—<?php endif; ?>
						<?php echo ($vo["name"]); ?>
						</option>
					<?php else: ?>
						<option value="<?php echo ($vo["term_id"]); ?>">
						<?php $arr = explode('-', $vo['path'] ); for ($i=0; $i < count($arr); $i++) { echo '&nbsp;&nbsp;&nbsp;'; } ?>
				    	<?php if( $vo['parent'] != 0): ?>|—<?php endif; ?>
						<?php echo ($vo["name"]); ?>
						</option><?php endif; endforeach; endif; ?>
				</select>
				&nbsp;&nbsp;&nbsp;时间： 
				<input type="text" name="post_date" class="laydate-icon" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"style="width: 170px;" >
				->
				<input type="text" name="post_date" class="laydate-icon" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"style="width: 170px;" >
				&nbsp;&nbsp;&nbsp;关键字： 
				<input type="text" name="post_title" style="width: 200px;" placeholder="关键字" id="search">
				<input type="submit" class="btn btn-primary" value="搜索">
				<a class="btn btn-danger" href="<?php echo U('video/index');?>" id="clear">清空</a>
			</form>
			<div class="panel-body no-padding" style="display: block;">
				<table class="table table-striped table-hover table-bordered" align="center" >
					<thead>
						<tr class="success" valign="middle">
							<th>#</th>
							<th>标题</th>
							<th>作者</th>
							<th>点击量/点赞量/收藏量/评论量</th>
							<th>关键字/来源/摘要/缩略图</th>
							<th>发布时间</th>
							<th style="width:180px;">状态</th>
							<th style="width:220px;">操作</th>	
						</tr>
					</thead>
					<tbody>
						<?php if(is_array($data)): foreach($data as $key=>$vo): ?><tr>
								<td><?php echo ($vo["id"]); ?></td>
								<td><a href="/thinkphp3-demo/Public/common/video/<?php echo ($vo['video']); ?>"><?php echo ($vo["post_title"]); ?></a></td>
								<td><?php echo ($vo["post_author"]); ?></td>
								<td><?php echo ($vo["post_hits"]); ?>/<?php echo ($vo["post_like"]); ?>/<?php echo ($vo["post_collect"]); ?>/<?php echo ($vo["comment_count"]); ?></td>
								<td>
									<?php if($vo['post_keywords']): ?>有/
									<?php else: ?>
										无/<?php endif; ?>
									<?php if($vo['post_source']): ?>有/
									<?php else: ?>
										无/<?php endif; ?>
									<?php if($vo['post_excerpt']): ?>有/
									<?php else: ?>
										无/<?php endif; ?>
									<?php if($vo['post_one_image']): ?><a href="/thinkphp3-demo/Public/common/images/<?php echo ($vo['post_one_image']); ?>">查看</a>
									<?php else: ?>
										无<?php endif; ?>
								</td>
								<td><?php echo ($vo["post_date"]); ?></td>
								<td>
									<?php if($vo['post_status'] == 1): ?>已审核/
									<?php else: ?>
										未审核/<?php endif; ?>
									<?php if($vo['istop']): ?>已置顶/
									<?php else: ?>
										未置顶/<?php endif; ?>
									<?php if($vo['recommended']): ?>已推荐
									<?php else: ?>
										未推荐<?php endif; ?>
								</td>
								<td valign="middle">
									<a href="<?php echo U('video/post_status', array('id'=>$vo['id'], 'post_status'=>$vo['post_status']));?>" class="btn btn-primary btn-xs">
										<?php if($vo['post_status']): ?>取消
										<?php else: ?>
											审核<?php endif; ?>
									</a>
									<a href="<?php echo U('video/istop', array('id'=>$vo['id'], 'istop'=>$vo['istop']));?>" class="btn btn-primary btn-xs">
										<?php if($vo['istop']): ?>取消
										<?php else: ?>
											置顶<?php endif; ?>
									</a>
									<a href="<?php echo U('video/recommended', array('id'=>$vo['id'], 'recommended'=>$vo['recommended']));?>" class="btn btn-primary btn-xs">
										<?php if($vo['recommended']): ?>取消
										<?php else: ?>
											推荐<?php endif; ?>
									</a> |
									<a href="<?php echo U('video/edit',array('id'=>$vo['id']));?>" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span></a> |
									<a href="<?php echo U('video/delete',array('id'=>$vo['id']));?>" class="btn btn-danger btn-xs"><span class="fa fa-trash-o"></span></a>
								</td>
							</tr><?php endforeach; endif; ?>
					</tbody>
				</table>
				<div class="col-sm-12" id="page"><?php echo ($page); ?></div>
			</div>
		</div>
        <script src="/thinkphp3-demo/Public/admin/js/bootstrap.min.js"></script>
<script>
  //图片上传预览    IE是用了滤镜。
    function previewImage(file)
    {
      var MAXWIDTH  = 200; 
      var MAXHEIGHT = 200;
      var div = document.getElementById('preview');
      if (file.files && file.files[0])
      {
          div.innerHTML ='<img id=imghead onclick=$("#previewImg").click()>';
          var img = document.getElementById('imghead');
          img.onload = function(){
            var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
            img.width  =  rect.width;
            img.height =  rect.height;
//                 img.style.marginLeft = rect.left+'px';
            img.style.marginTop = rect.top+'px';
          }
          var reader = new FileReader();
          reader.onload = function(evt){img.src = evt.target.result;}
          reader.readAsDataURL(file.files[0]);
      }
      else //兼容IE
      {
        var sFilter='filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
        file.select();
        var src = document.selection.createRange().text;
        div.innerHTML = '<img id=imghead>';
        var img = document.getElementById('imghead');
        img.filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
        var rect = clacImgZoomParam(MAXWIDTH, MAXHEIGHT, img.offsetWidth, img.offsetHeight);
        status =('rect:'+rect.top+','+rect.left+','+rect.width+','+rect.height);
        div.innerHTML = "<div id=divhead style='width:"+rect.width+"px;height:"+rect.height+"px;margin-top:"+rect.top+"px;"+sFilter+src+"\"'></div>";
      }
    }
    function clacImgZoomParam( maxWidth, maxHeight, width, height ){
        var param = {top:0, left:0, width:width, height:height};
        if( width>maxWidth || height>maxHeight ){
            rateWidth = width / maxWidth;
            rateHeight = height / maxHeight;
            
            if( rateWidth > rateHeight ){
                param.width =  maxWidth;
                param.height = Math.round(height / rateWidth);
            }else{
                param.width = Math.round(width / rateHeight);
                param.height = maxHeight;
            }
        }
        param.left = Math.round((maxWidth - param.width) / 2);
        param.top = Math.round((maxHeight - param.height) / 2);
        return param;
    }
</script>
<script type="text/javascript">
  !function(){
    laydate.skin('danlan');//切换皮肤，请查看skins下面皮肤库
    laydate({elem: '#demo'});//绑定元素
  }();
  //日期范围限制
  var start = {
    elem: '#start',
    format: 'YYYY-MM-DD',
    min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
       end.min = datas; //开始日选好后，重置结束日的最小日期
       end.start = datas //将结束日的初始值设定为开始日
    }
  };
  var end = {
    elem: '#end',
    format: 'YYYY-MM-DD',
    min: laydate.now(),
    max: '2099-06-16',
    istime: true,
    istoday: false,
    choose: function(datas){
      start.max = datas; //结束日选好后，充值开始日的最大日期
    }
  };
  laydate(start);
  laydate(end);
  //自定义日期格式
  laydate({
    elem: '#test1',
    format: 'YYYY年MM月DD日',
    festival: true, //显示节日
    choose: function(datas){ //选择日期完毕的回调
      alert('得到：'+datas);
    }
  });
  //日期范围限定在昨天到明天
  laydate({
    elem: '#hello3',
    min: laydate.now(-1), //-1代表昨天，-2代表前天，以此类推
    max: laydate.now(+1) //+1代表明天，+2代表后天，以此类推
  });
</script>
    </div>
</div>
</body>
</html>