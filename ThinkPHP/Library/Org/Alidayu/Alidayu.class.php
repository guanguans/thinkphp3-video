<?php
namespace Org\Alidayu;
include "TopSdk.php";
use TopClient;
use AlibabaAliqinFcSmsNumSendRequest;
/**
 *  Alidayu实现类
 *  @author 琯琯
 *  date 2017.5.4
 */
class  Alidayu{

	/**
	*	错误
	*	SimpleXMLElement Object
	*	(
	*		[code] => 15
	*		[msg] => Remote service error
	*		[sub_code] => isv.MOBILE_NUMBER_ILLEGAL
	*		[sub_msg] => 号码格式错误
	*		[request_id] => el2xsitf4pte
	*	)
	*	
	*   成功
	*	SimpleXMLElement Object
	*	(
	*		[result] => SimpleXMLElement Object
	*			(
	*				[err_code] => 0
	*				[model] => 107290053503^1109820800486
	*				[success] => true
	*			)
	*
	*		[request_id] => iv13psln6adf
	*	)
	*
	* 	@param string $mobile 手机号
	* 	@param string $code 验证码
	*	@return array
	*/
	public function send_code($mobile, $code){
	    date_default_timezone_set('Asia/Shanghai'); 
		$json_data['time'] = date('H:i:s');
		$json_data['code'] = $code;
		$json_str = json_encode($json_data);
		$c = new TopClient;
		$c ->appkey = '23784802';
		$c ->secretKey = '3df3b5657c124a71cda4db362a424f5d';
		$req = new AlibabaAliqinFcSmsNumSendRequest;
		$req ->setExtend( "" );//扩展附件说明
		$req ->setSmsType( "normal" );
		$req ->setSmsFreeSignName( "姚照明" );
		$req ->setSmsParam( $json_str );
		$req ->setRecNum( $mobile );
		$req ->setSmsTemplateCode( "SMS_63820612" );
		$resp = $c ->execute( $req ); //对象
		$result = json_decode(json_encode($resp), true);
		// if ($result['result']['success']) {
		// 	echo '11111111';
		// } else {
		// 	echo $result['sub_msg'];
		// }
		return $result;
	}
 

}
	